Запуск приложения-примера SetSale
===============

1. Загрузите приложение

```
git clone https://gitlab.com/setsale/example-app
```

2. Скопируйте файл .env.example, переименуйте копию в .env и отредактируйте, настроив параметры доступа к базе данных. 

В качестве базы данных можно, например, использовать SQLite. Для этого создайте пустой файл db.sqlite, а в .env замените следующие значения: 

```
DB_CONNECTION=sqlite
DB_DATABASE=<путь к файлу db.sqlite>
```

3. Выполните команды для установки приложения: 

```
composer update
php artisan key:generate
php artisan migrate
```