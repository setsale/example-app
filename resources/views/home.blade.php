@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $title }}</div>

                <div class="card-body">
                    @if ($categoriesCount >= 0)
                        There are {{ $categoriesCount }} categories in the store.
                    @else
                        Can not get data from store.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
