<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Set Sale host
	|--------------------------------------------------------------------------
	|
	| This value is the host of Set Sale auth server. This value is used for
	| requests and redirects when authorizing a user.
	|
	*/

	'host' => 'https://auth.setsale.ru',
];