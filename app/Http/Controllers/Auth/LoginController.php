<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

	/**
	 * Show the application's login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showLoginForm()
	{
		return view('auth.login');
	}

	/**
	 * Redirect to Set Sale for authorization.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function oauthRedirect(Request $request)
	{
		return redirect(config('setsale.host') . '/oauth/authorize?redirect_url=' . route('oauth.login', [], true));
	}

	/**
	 * Handle redirection from Set Sale after successful login.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function oauthLogin(Request $request)
	{
		$this->validate($request, [
			'shop_id' => 'required|numeric|between:0,999999',
			'token' => 'required|string',
		]);

		$guzzleClient = new \GuzzleHttp\Client(['http_errors' => false]);
		$response = $guzzleClient->request('GET', config('setsale.host') . '/api/v1/shops/' . $request->query('shop_id') . '/api-key?token=' . $request->query('token'));

		if ($response->getStatusCode() === Response::HTTP_OK) {
			$apiKeyData = \GuzzleHttp\json_decode($response->getBody());

			$user = User::where('shop_id', $request->query('shop_id'))->first();
			if (!$user) {
				$user = new User();
				$user->shop_id = $request->query('shop_id');
			}
			$user->shop_basic_domain = $apiKeyData->basicDomain;
			$user->shop_api_key = $apiKeyData->adminApiKey;
			$user->save();

			Auth::login($user);

			return redirect()->route('home');
		} else {
			return redirect()->route('login');
		}
	}

	/**
	 * Log the user out of the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function logout(Request $request)
	{
		Auth::guard()->logout();

		$request->session()->invalidate();

		return redirect()->route('login');
	}
}
