<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Show application home page.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
    public function index()
    {
    	$user = Auth::user();

		$guzzleClient = new \GuzzleHttp\Client(['http_errors' => false]);
		$requestOptions = [
			'headers' => [
				'Authorization' => 'Bearer ' . $user->shop_api_key,
			],
		];
		$response = $guzzleClient->request('GET', $user->shop_basic_domain . '/cp/api/v2/categories', $requestOptions);

		$categoriesCount = -1;
		if ($response->getStatusCode() === Response::HTTP_OK) {
			$data = \GuzzleHttp\json_decode($response->getBody());
			$categoriesCount = $data->paging->total;
		}

        return view('home', [
        	'title' => $user->shop_basic_domain,
			'categoriesCount' => $categoriesCount,
		]);
    }
}
